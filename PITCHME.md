# Sitting / standing tracking

---

## What does the science say?

---

**The impact of sit-stand office workstations on worker discomfort and productivity: a review** (_Karakolis T, Callaghan JP.; 2013_)
- "sit-stand workstations are likely effective in reducing perceived discomfort"
- "sit-stand workstations do not cause a decrease in productivity"

---

**Sit–stand desks in call centres: Associations of use and ergonomics awareness with sedentary behavior** (_Straker L. et al.; 2012_)
- "use of sit–stand desks was associated with better sedentary behavior"

---

**“Thinking on your feet”: A qualitative evaluation of sit-stand desks in an Australian workplace** (_Grunseit A. et al.; 2013_)
- "may provide a practical and acceptable means of reducing sedentary time among some office workers"
- "sit-stand desks...reduced sitting time at work"

---

## What does the science say? 🤷

---

- Sitting for too long is (probably<sup>†</sup>) bad
- Standing for too long is (probably<sup>†</sup>) bad

<sup>†</sup> I'm not a doctor

---

## How long do I sit / stand for each day?

---

## STFU

- macOS utility
- Tracking: standing, sitting, paused
- Auto-pauses/resumes on: sleep/wake, display sleep/wake
- Notifications
- Export to cabot@<sup>†</sup>
- Basic data viewer

<sup>†</sup> Sorry, 💩 joke

---

## (demo)

---

|      | Sitting | Standing |
|-----:|---------|----------|
| Min  | 02h 23m | 01h 54m  |
| Max  | 05h 53m | 05h 32m  |
| Mean | 03h 58m | 03h 54m  |

---?image=assets/daily-totals-hist.png&size=contain

---?image=assets/interval-hist.png&size=contain

---?image=assets/sitting-standing.png&size=contain

---?image=assets/away-present.png&size=contain
